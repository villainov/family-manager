I2P family manager
==================

Features:
--------

- Generate family keys
- Massively deploy any amount of your family nodes to remote hosts
- Supports Debian 8/9 and Ubuntu 16.04/18.04 servers


Requirements:
-------------

On local host:

- Ansible >= 2.4
- openssl
- OpenJDK (for keytool utility)
- Bouncy Castle (elliptic curves crypto provider for keytool)

For Ubuntu/Debian, packages are `ansible openssl openjdk-8-jdk libbcprov-java`

On remote server(s):

- Debian 8/9 or Ubuntu 16.04/18.04
- root/sudo account required with configured SSH keys

Usage
-----

1) Generate keystore

    ./family-manager keygen

It will ask you for your family name, and create the following files:

- keystore/family-\<family\_name\>.ks
- certificates/family/\<family\_name\>.crt
- .config

2) Edit .config file if you need to. Available settings:

- BECOME, set to BECOME=yes if you use a regular user with sudo on a remote host 
- PYTHON2, set to PYTHON2=yes if remote host doesn't have python3 (e.g. Debian 8)
- ANSIBLE\_HOST\_KEY\_CHECKING, set to ANSIBLE\_HOST\_KEY\_CHECKING=False, if you want to skip SSH fingerprints verification for every remote host

3) Deploy your nodes. There are 2 options:

- Specify remote hosts as a comma separated list, like:


    ./family-manager deploy 123.4.5.6,some.hostname.com,other.host.name

- Specify Ansible inventory file, which has special .ini syntax:

```
[all]
123.4.5.6
some.hostname.com
other.host.name
```

then run the following:

    ./family-manager deploy inventory.ini

4) Update your nodes periodically:

    ./family-manager update 123.4.5.6,some.hostname.com,other.host.name
    
or

    ./family-manager update inventory.ini
